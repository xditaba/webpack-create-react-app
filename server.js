const express = require("express");
const path = require("path");
const app = express();
console.log("[server start]", __dirname);
app.use(express.static(path.join(__dirname, "build")));
app.get("/*", function(req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
  // res.sendFile(path.join(__dirname, 'release', 'build_v0.0.8', 'index.html'));
});
app.listen(9001);
